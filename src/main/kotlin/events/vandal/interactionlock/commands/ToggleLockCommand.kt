package events.vandal.interactionlock.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.interactionlock.tables.Lock
import events.vandal.interactionlock.tables.Locked
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

object ToggleLockCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val block = sender.getTargetBlock(5)

        if (block == null || block.type.isAir) {
            sender.sendMessage(
                ChatColor.translateAlternateColorCodes(
                    '&',
                    "&8[&c!&8] &cYou have to be directly looking at the block!"
                )
            )

            return
        }

        val lock = transaction {
            return@transaction Lock.find {
                (Locked.locationX eq block.x) and
                (Locked.locationY eq block.y) and
                (Locked.locationZ eq block.z)
            }.firstOrNull() ?: Lock.new {
                locked = false
                locationX = block.x
                locationY = block.y
                locationZ = block.z
            }
        }

        transaction {
            lock.locked = !lock.locked
        }

        val message = if (lock.locked) {
            "&8[&a+&8] &7Locked ${block.x}, ${block.y}, ${block.z}"
        } else {
            "&8[&c-&8] &7Unlocked ${block.x}, ${block.y}, ${block.z}"
        }

        sender.sendMessage(
            ChatColor.translateAlternateColorCodes(
                '&',
                message
            )
        )
    }
}