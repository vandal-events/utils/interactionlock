package events.vandal.interactionlock.events

import events.vandal.interactionlock.tables.Lock
import events.vandal.interactionlock.tables.Locked
import org.bukkit.Material
import org.bukkit.entity.Hanging
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.player.PlayerInteractEntityEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

class InteractionEvent : Listener {
    @EventHandler
    fun onItemFrameClick(event: PlayerInteractEntityEvent) {
        if (event.rightClicked is Hanging) {
            if (!event.player.hasPermission("interactionlock.admin")) {
                event.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onItemFrameAttack(event: EntityDamageByEntityEvent) {
        if (event.entity is Hanging) {
            if (!event.damager.hasPermission("interactionlock.admin")) {
                event.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onBlockInteract(event: PlayerInteractEvent) {
        when {
            event.clickedBlock != null || event.action == Action.PHYSICAL -> {
                val block = event.clickedBlock ?: return

                val lock = transaction {
                    return@transaction Lock.find {
                        (Locked.locationX eq block.x) and
                        (Locked.locationY eq block.y) and
                        (Locked.locationZ eq block.z)
                    }.firstOrNull()
                } ?: return

                if (lock.locked) {
                    event.isCancelled = true
                }
            }

            event.action == Action.PHYSICAL && event.clickedBlock?.type == Material.FARMLAND -> {
                event.isCancelled = true
            }
        }
    }
}