package events.vandal.interactionlock.tables

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object Locked : IntIdTable() {
    val locked: Column<Boolean> = bool("locked")
    val locationX: Column<Int> = integer("location_x")
    val locationY: Column<Int> = integer("location_y")
    val locationZ: Column<Int> = integer("location_z")
}

class Lock(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Lock>(Locked)
    var locked by Locked.locked
    var locationX by Locked.locationX
    var locationY by Locked.locationY
    var locationZ by Locked.locationZ
}
